'''
A script that takes in csv course data from Dana, and sends back an 
email with any new professors that are offerring courses.
'''

import smtplib
import time
import imaplib
import email
import csv

from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEBase import MIMEBase
from email import encoders


FROM_EMAIL  = "getintouchwithisaac@gmail.com"
FROM_PWD    = "antialiasis321"
SMTP_SERVER = "imap.gmail.com"
SMTP_PORT   = 993

def add_area(new_faculty):
    with open("area_list.csv") as file:
        a = csv.DictReader(file)
        areas = list(a)

    for member in new_faculty:
        for course in areas:
            if course["Course Title"] == member["Course Title"]:
                member["Course Area"] = course["Area"]
                break
	    else:
	      member["Course Area"] = "Unknown"
    return new_faculty


def read_email():
    new_faculty = []

    mail = imaplib.IMAP4_SSL(SMTP_SERVER)
    mail.login(FROM_EMAIL,FROM_PWD)
    mail.select('inbox')

    a, data = mail.search(None, "UNSEEN")

    for email_number in reversed(data[0].split()):
    	typ, msg = mail.fetch(email_number,'(RFC822)')
    	m = email.message_from_string(msg[0][1])
    	
    	for part in m.walk():
            if part.get_content_type() == "text/csv" and "csv" in m["subject"].lower():
                a = part.get_payload(decode=True)
                with open("temp.csv", "w") as file:
                    file.write(a)
                with open("temp.csv") as file:
                    a = csv.DictReader(file)
                    a = list(a)


                new_faculty.append(check_against_previous_faculty(a))


    return new_faculty
        			


def check_against_previous_faculty(new_data):
    new_faculty = []

    for row in new_data:
        found = False
        instructor_id = row["Instructor Tuid"]

        with open("all_faculty.csv") as file:
            all_faculty = csv.DictReader(file)
            all_faculty = list(all_faculty)


        for faculty_member in all_faculty:
            if faculty_member["Instructor Tuid"] == instructor_id:
                found = True
                break
		
        for each in new_faculty:
            if each["Instructor Tuid"] == instructor_id:
                found = True	

        try:
	        if (not found) and (int(row["Course Number"]) >= 800 and int(row["Course Number"]) <= 999) and row["Course Subject"] != "Juris Doctor":
	            new_faculty.append({"Instructor Tuid" : instructor_id, "Instructor Name": row["Instructor Name"], "Instructor Email": row["Instructor Email"], "Course Title": row["Course Title"], "Course Number": row["Course Number"], "Term Code": row["Term"]})
        except:
        	continue

    return new_faculty

def send_mail(new_faculty):
    msg = MIMEMultipart()
     
    msg['From'] = FROM_EMAIL
    msg['To'] = FROM_EMAIL
    msg['Subject'] = "New Faculty Members"
     
    #Creates body of message, formatting the data prettily, sorting by area code
    body = "New Faculty: \n\n"
    areas = {"GD" : [], "GU" : [], "GG" : [], "GA" : [], "GB" : [], "GQ" : [], "GS" : [], "GW": [], "GY" : [], "GZ" : []}
    others = []
    for faculty_member in new_faculty:
        a = ("Instructor Name: {0},  Instructor TUid: {1}, Instructor Email: {2}, Course Title: {3}, Course Number {4}, Term Code: {5}, Course Area: {6}\n\n").format(faculty_member["Instructor Name"], faculty_member["Instructor Tuid"],faculty_member["Instructor Email"], faculty_member["Course Title"], faculty_member["Course Number"], faculty_member["Term Code"], faculty_member["Course Area"])
    	if (faculty_member["Course Area"] in areas.keys()):
            areas[faculty_member["Course Area"]].append(a)
        else:
            others.append(a)

    for area, courses in areas.items():
        body += "<b>Course Area " + area + ":</b>\n"
        for course in courses:
            body += course


    body += "<b>Could not find course area:</b>\n"
    for course in others:
        body += course


     
    msg.attach(MIMEText(body, 'plain'))
     
    filename = "all_faculty.csv"
    attachment = open("./all_faculty.csv", "rb")
     
    part = MIMEBase('application', 'octet-stream')
    part.set_payload((attachment).read())
    encoders.encode_base64(part)
    part.add_header('Content-Disposition', "attachment; filename= %s" % filename)
     
    msg.attach(part)
     
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login(FROM_EMAIL, FROM_PWD)
    text = msg.as_string()
    server.sendmail(FROM_EMAIL, FROM_EMAIL, text)
    server.quit()


new_faculty = read_email()
if new_faculty != [[]] and new_faculty != []:
    new_faculty = add_area(new_faculty[0])
    with open("all_faculty.csv", "a+") as export:
        w = csv.DictWriter(export, new_faculty[0].keys())
        w.writerows(new_faculty)
    send_mail(new_faculty)




'''
#Pulls data from Kevin's original all_faculty file, should now be irrelevant

with open("all_faculty_cp.csv") as file:
	a = csv.DictReader(file)
	a = list(a)

all_faculty = []

for each in a:

	all_faculty.append({"Instructor Name" : each["Instructor First Name"] + " " + each["Instructor Last Name"], "Instructor Tuid" : each["Instructor ID"], "Course Title" : each["Course Title"], "Course Number" : each["Course Number (for sorting and filtering)"], "Instructor Email" : each["Instructor Email Address"], "Term Code": each["Term Code"]} )

	with open("all_faculty.csv", "w") as export:
		w = csv.DictWriter(export, all_faculty[0].keys())
		w.writeheader()
		w.writerows(all_faculty)
'''
